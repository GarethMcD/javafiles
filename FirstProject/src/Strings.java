public class Strings {


    public static void main(String[] args) {
        String doc = "example.doc";
        doc = doc.replace("doc","bak");
        System.out.println(doc);

       String string1 = "Test1";
       String string2 = "Test";

       int result = string1.compareTo(string2);
       System.out.println(result);

       if (result > 0)
       {
           System.out.println(string1);
       }
       else {
           System.out.println(string2);
       }

       String sentence = "the quick brown fox swallowed down the lazy chicken";
       String findStr = "ow";
       System.out.println(sentence.split(findStr, -1).length-1);

       String palindrome = "racecar";

       if (isPalindrome(palindrome))
           // It is a pallindrome
           System.out.print("Yes");
       else

           // Not a pallindrome
           System.out.print("No");


    }

    static boolean isPalindrome(String str)
    {

        int i = 0, j = str.length() - 1;

        while (i < j) {


            if (str.charAt(i) != str.charAt(j))
                return false;


            i++;
            j--;
        }

        // Given string is a palindrome
        return true;
    }
}
